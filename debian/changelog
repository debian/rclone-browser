rclone-browser (1.8.0-5) unstable; urgency=medium

  * Bump standards version to 4.7.0,

 -- Alex Myczko <tar@debian.org>  Mon, 15 Jul 2024 06:14:42 +0000

rclone-browser (1.8.0-4) unstable; urgency=medium

  * Upload to unstable.

 -- Gürkan Myczko <tar@debian.org>  Tue, 13 Jun 2023 16:23:00 +0200

rclone-browser (1.8.0-3) experimental; urgency=medium

  * Bump standards version to 4.6.2.
  * d/control: update Vcs fields.
  * d/upstream/metadata: added.

 -- Gürkan Myczko <tar@debian.org>  Fri, 31 Mar 2023 07:25:44 +0200

rclone-browser (1.8.0-2) unstable; urgency=medium

  * Update maintainer address.
  * Depend on rclone for mipsel, drop Suggests. (Closes: #972546)
  * d/control: Update Vcs fields.
  * Bump debhelper version to 13, drop d/compat.
  * Bump standards version to 4.6.0.

 -- Gürkan Myczko <tar@debian.org>  Tue, 19 Apr 2022 07:29:15 +0200

rclone-browser (1.8.0-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Build without -Werror. (Closes: #973786)

 -- Adrian Bunk <bunk@debian.org>  Sat, 06 Feb 2021 21:31:46 +0200

rclone-browser (1.8.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Don't Depend on rclone on mipsel (where rclone fails to build).
    Use Suggests instead. See Bug#972546.

 -- Drew Parsons <dparsons@debian.org>  Sun, 25 Oct 2020 10:34:08 +0800

rclone-browser (1.8.0-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.5.0.
  * Add Vcs fields.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 18 Feb 2020 04:58:09 +0100

rclone-browser (1.7.0-1) unstable; urgency=medium

  * New upstream version.
  * d/README.source: dropped, fixed upstream.
  * Update homepage.
  * Bump standards version to 4.4.1.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 17 Dec 2019 07:25:41 +0100

rclone-browser (1.2-1) unstable; urgency=medium

  * Initial release. (Closes: #920138)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 22 Jan 2019 04:31:02 +0100
